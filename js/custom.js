jQuery(document).ready(function(){
    jQuery('.banner-slider').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1
    });
    /*jQuery('.v-slider ').slick({
        dots: true,
        vertical: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        verticalSwiping: true
    });*/
    jQuery('.review-slider').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1
    });
    jQuery('.team-slider-wrapper').slick({
        centerMode: true,
        centerPadding: '30px',
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '60px',
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
        jQuery('.accordion_body').css('display' , 'none');
        //toggle the component with class accordion_body
        jQuery(".accordion_head").click(function() {
            if (jQuery('.accordion_body').is(':visible')) {
                jQuery(".accordion_body").slideUp(400);
                jQuery(".plusminus").removeClass('minus').text('+');
            }
            if (jQuery(this).next(".accordion_body").is(':visible')) {
                jQuery(this).next(".accordion_body").slideUp(400);
                jQuery(this).children(".plusminus").removeClass('minus').text('+');
            } else {
                jQuery(this).next(".accordion_body").slideDown(400);
                jQuery(this).children(".plusminus").addClass('minus').text('-');
            }
        });
        jQuery(".expand-all").click(function () {
            jQuery('.accordion_head').next(".accordion_body").slideDown(400);
            jQuery('.accordion_head').children('.plusminus').addClass('minus').text('-');
        });
        jQuery(".contract-all").click(function () {
            jQuery('.accordion_head').next(".accordion_body").slideUp(400);
            jQuery('.accordion_head').children('.plusminus').removeClass('minus').text('+');
        });
    jQuery(window).scroll(function() {
        var scroll = jQuery(window).scrollTop();

        if (scroll >= 100) {
            jQuery(".sticky-top").addClass("submenu-top");
        } else {
            jQuery(".sticky-top").removeClass("submenu-top");
        }
    });

    jQuery(".footer-wrapper h4").click(function(){
        /*jQuery(this).parent(".link-list").toggleClass("open"); */
        jQuery(this).parent(".footer-col").toggleClass("open");

        if(jQuery(window).width()<=767){
            jQuery(this).next(".footer-col ul").slideToggle("slow");
        }
    });

    if ( jQuery(document).width() < 769 ) {
        jQuery('.ivynav .nav-justified .nav-item.dropdown').click(function(){
            jQuery(this).children('.menu').slideToggle('slow');
            jQuery(this).toggleClass('nav-slide');
        });
    }

    //contact us modal pagination
    jQuery('.pagination li').click(function(){
        var tab_id = $(this).attr('data-tab');

        jQuery('.pagination li').removeClass('current');
        jQuery('.tabcontent').removeClass('current');

        jQuery(this).addClass('current');
        jQuery("#"+tab_id).addClass('current');
    })
});


